import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_host(host):
    print(host)

@pytest.mark.parametrize("locale", [
    ("en_US.utf8"),
    ("en_GB.utf8"),
    ("sv_SE.utf8"),
    ("zh_CN.utf8"),
])
def test_locales(host, locale):
    cmd = host.run("locale -a")
    assert cmd.rc == 0
    assert locale in cmd.stdout


@pytest.mark.parametrize("name", [
    ("locales"),
    ("tmux"),
    ("tmuxinator"),
    ("screen"),
    ("bash-completion"),
    ("vlock"),
    ("mutt"),
    ("mc"),
    ("htop"),
    ("iotop"),
    ("members"),
    ("git"),
    ("python3"),
    ("ssh"),
    ("sshfs"),
    ("mosh"),
    ("emacs-nox"),
    ("nano"),
    ("vim"),
    ("joe"),
    ("ispell"),
    ("iswedish"),
    ("ienglish-common"),
    ("iamerican"),
    ("p7zip-full"),
    ("unrar"),
    ("colordiff"),
    ("toilet"),
])
def test_packages(host, name):
    pkg = host.package(name)
    assert pkg.is_installed


def test_version_specific_packages(host):
    if host.system_info.release == "20.04":
        assert host.package("elpa-yaml-mode").is_installed
    else:
        assert host.package("yaml-mode").is_installed


@pytest.mark.parametrize("command,expected_rc_16,expected_rc_18,expected_rc_20", [
    ("tmux -V", 0, 0, 0),
    ("screen -v", 1, 1, 0),
    ("git --version", 0, 0, 0),
    ("python3 --version", 0, 0, 0),
    ("vlock --version", 0, 0, 0),
    ("emacs --version", 0, 0, 0),
    ("vim --version", 0, 0, 0),
    ("nano --version", 0, 0, 0),
    ("mutt -v", 0, 0, 0),
    ("ispell -v", 0, 0, 0),
    ("yadm help", 1, 1, 1),
    ("toilet --version", 0, 0, 0),
    ("unrar -v", 0, 0, 0),
    ("7z", 0, 0, 0),
    ("colordiff --version", 0, 0, 0),
    ("sshfs --version", 0, 0, 0),
    ("mc --version", 0, 0, 0),
])
def test_programs(host, command, expected_rc_16, expected_rc_18, expected_rc_20):
    cmd = host.run(command)
    expected_rc = { "16.04": expected_rc_16, "18.04": expected_rc_18, "20.04": expected_rc_20 }
    assert cmd.rc == expected_rc[host.system_info.release]

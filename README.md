# interactive_terminal

Ansible role to install and configure basic tools needed on servers
where people login and make work on the terminal.

It installes editors, man, auto-completion, tmux and other command
line tools. It also sets up locales (US/SE) and spell-checking.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

_None_

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: interactive_terminal
```

## License

BSD 2-clause
